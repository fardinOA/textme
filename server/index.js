const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose');
const app = express();

app.use(express.json())

//mongo connection
const url=process.env.MONGO_URL 
mongoose.connect(url,{
    useNewUrlParser:true,
    useUnifiedTopology:true
}).then(console.log("server connected"))
    .catch(err => {
        console.log(err);
    })


const port=process.env.PORT||3030
app.get('/',(req,res)=>{
    res.send(`<h1>I am from root</h1>`);
})

app.listen(port,console.log('Running on port ',port));